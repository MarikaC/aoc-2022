use std::fs;
use std::env;
use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;

enum FileType {
    Folder,
    Text
}

struct File {
    parent: Option<Rc<RefCell<File>>>,
    file_type: FileType,
    children: FileStructure,
    size: Option<u32>
}

impl File {
    fn new_folder() -> Self {
        File {
            parent: None,
            file_type: FileType::Folder,
            children: FileStructure::new(),
            size: None
        }
    }

    fn new_text_file(size: u32) -> Self {
        File {
            parent: None,
            file_type: FileType::Text,
            children: FileStructure::new(),
            size: Some(size)
        }
    }
    
    fn calculate_size(&mut self) {
        let mut size = 0;
        for (_,  child) in &self.children {
            let mut child = child.borrow_mut();
            if child.size == None {
                child.calculate_size();
            }
            size += child.size.unwrap();
        }
        self.size = Some(size);
    }
}

type FileStructure = HashMap<String, Rc<RefCell<File>>>;

struct FileStructureEditor {
    current_file: Rc<RefCell<File>>,
    root_file: Rc<RefCell<File>>
}

impl FileStructureEditor {
    fn new(root: &Rc<RefCell<File>>) -> Self {
        FileStructureEditor {
            current_file: Rc::clone(&root),
            root_file: Rc::clone(&root)
        }
    }

    fn add_child(&mut self, name: &str, file: &Rc<RefCell<File>>) {
        file.borrow_mut().parent = Some(Rc::clone(&self.current_file));
        self.current_file.borrow_mut().children.insert(name.to_string(), Rc::clone(&file));
    }

    fn go_to_child(&mut self, name: &str) {
        let current_file = Rc::clone(&self.current_file.borrow().children[name]);
        self.current_file = current_file;
    }

    fn go_to_root(&mut self) {
        self.current_file = Rc::clone(&self.root_file)
    }

    fn go_to_parent(&mut self) {
        let current_file = Rc::clone(&self.current_file.borrow().parent.as_ref().unwrap());
        self.current_file = current_file;
    }
}

fn get_folders_with_max_size(file: &Rc<RefCell<File>>, size: u32) -> Vec<Rc<RefCell<File>>> {
    let mut result = Vec::new();
    let current_file = file.borrow();
    match current_file.file_type {
        FileType::Folder => {
            if current_file.size.unwrap() <= size {
                result.push(Rc::clone(&file));
            }
            for (_, child) in &current_file.children {
                result.append(&mut get_folders_with_max_size(&child, size));
            }
        }
        FileType::Text => ()
    }
    return result;
}

fn get_folders_with_min_size(file: &Rc<RefCell<File>>, size: u32) -> Vec<Rc<RefCell<File>>> {
    let mut result = Vec::new();
    let current_file = file.borrow();
    match current_file.file_type {
        FileType::Folder => {
            if current_file.size.unwrap() >= size {
                result.push(Rc::clone(&file));
            }
            for (_, child) in &current_file.children {
                result.append(&mut get_folders_with_min_size(&child, size));
            }
        }
        FileType::Text => ()
    }
    return result;
}

fn calculate_required_space(system_space: u32, needed_space: u32, root_directory: &Rc<RefCell<File>>) -> u32 {
    root_directory.borrow().size.unwrap() + needed_space - system_space
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let contents = contents[2..].to_string();
    
    let commands = contents.split("\n$ ");

    let root_file = Rc::new(RefCell::new(File::new_folder()));
    let mut structure_editor = FileStructureEditor::new(&root_file);

    for command in commands {
        if command.starts_with("cd") {
            let (_, target) = command.split_once(' ').unwrap();
            match target {
                "/" => {
                    structure_editor.go_to_root();
                }
                ".." => {
                    structure_editor.go_to_parent();
                }
                child => {
                    structure_editor.go_to_child(&child)
                }
            }
        }
        else if command.starts_with("ls") {
            let mut file_lines = command.lines();
            file_lines.next();
            for line in file_lines {
                let (size, name) = line.split_once(' ').unwrap();
                let new_file: Rc<RefCell<File>>;
                if size == "dir" {
                    new_file = Rc::new(RefCell::new(File::new_folder()));
                }
                else {
                    new_file = Rc::new(RefCell::new(File::new_text_file(size.parse().unwrap())))
                }
                structure_editor.add_child(&name, &new_file);
            }
        }
    }

    root_file.borrow_mut().calculate_size();

    match part_of_task {
        1 => {
            let files = get_folders_with_max_size(&root_file, 100000);
            let mut size_sum = 0;
            for file in files {
                size_sum += file.borrow().size.unwrap();
            }
            println!("{}", size_sum);
        }
        2 => {
            let required_space = calculate_required_space(70000000, 30000000, &root_file);
            let files = get_folders_with_min_size(&root_file, required_space);
            let min_file = files.iter().min_by(|folder1, folder2| folder1.borrow().size.unwrap().cmp(&folder2.borrow().size.unwrap()));
            println!("{}", min_file.unwrap().borrow().size.unwrap())
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
