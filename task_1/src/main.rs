use std::fs;
use std::env;

type ElfBackpack = Vec<i32>;

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let elf_inputs: Vec<&str> = contents.split("\n\n").collect();
    let mut elf_backpacks = Vec::<ElfBackpack>::new();
    for input in elf_inputs {
        let meals: Vec<&str> = input.split("\n").collect();
        let mut elf_backpack = ElfBackpack::new();
        for meal in meals {
            if let Ok(calories) = meal.parse() {
                elf_backpack.push(calories);
            };
        }
        elf_backpacks.push(elf_backpack);
    }

    let backpack_comparator1 = |backpack1: &&ElfBackpack, backpack2: &&ElfBackpack| {
        backpack1.iter().sum::<i32>().cmp(&backpack2.iter().sum::<i32>())
    };

    let backpack_comparator2 = |backpack1: &ElfBackpack, backpack2: &ElfBackpack| {
        backpack1.iter().sum::<i32>().cmp(&backpack2.iter().sum::<i32>())
    };

    match part_of_task {
        1 => {
            let max_calories = elf_backpacks.iter().max_by(backpack_comparator1).unwrap().iter().sum::<i32>();
            println!("{}", max_calories);
        }
        2 => {
            elf_backpacks.sort_by(backpack_comparator2);
            let (_, top3elfs) = elf_backpacks.split_at(elf_backpacks.len() - 3);
            let mut top3elfs_calories = 0;
            for elf_calories in top3elfs {
                top3elfs_calories += elf_calories.iter().sum::<i32>();
            }
            println!("{}", top3elfs_calories);
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
