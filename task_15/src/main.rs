use std::fs;
use std::env;
use std::collections::{HashSet, HashMap};

type Coordinates = (i64, i64);

struct BeaconMap {
    sensor_to_range: HashMap<Coordinates, i64>,
    beacons: HashSet<Coordinates>
}

impl BeaconMap {
    fn new() -> Self {
        BeaconMap {
            sensor_to_range: HashMap::new(),
            beacons: HashSet::new()
        }
    }

    fn add_sensor_with_beacon(&mut self, sensor: &Coordinates, beacon: &Coordinates) {
        let sensor_range = manhattan_distance(&sensor, &beacon);
        self.sensor_to_range.insert(*sensor, sensor_range);
        self.beacons.insert(*beacon);
    }

    fn get_beacon_free_positions_along_horizontal_line(&self, line: i64) -> HashSet<Coordinates> {
        let mut result = HashSet::new();
        for (sensor, range) in &self.sensor_to_range {
            let horizontal_span = range - distance_to_horizontal_line(&sensor, line);
            for x in sensor.0-horizontal_span..=sensor.0+horizontal_span {
                result.insert((x, line));
            }
        }
        let result: HashSet<Coordinates> = result.difference(&self.beacons).map(|&coordinates| coordinates.clone()).collect();
        return result;
    }

    fn get_beacon_position_in_area(&self, max_coordinate: i64) -> Option<Coordinates> {
        // for x in 0..=max_coordinate {
        //     'mainloop: for y in 0..=max_coordinate {
        //         let point = (x, y);
        //         for (sensor, range) in  &self.sensor_to_range {
        //             if manhattan_distance(sensor, &point) <= *range {
        //                 continue 'mainloop;
        //             }
        //         }
        //         return Some(point);
        //     }
        // }
        // return None;
        let mut checked_positions = HashSet::new();
        let mut found;
        for (sensor, range) in  &self.sensor_to_range {
            let mut current_position = (sensor.0 + range + 1, sensor.1);
            while current_position.0 != sensor.0 {
                if current_position.0 <= max_coordinate && current_position.1 <= max_coordinate && current_position.0 >= 0 &&current_position.1 >= 0 && !checked_positions.contains(&current_position) {
                    found = true;
                    for (other_sensor, other_range) in &self.sensor_to_range {
                        if manhattan_distance(&current_position, &other_sensor) < *other_range {
                            checked_positions.insert(current_position);
                            found = false;
                            break;
                        }
                    }
                    if found {
                        return Some(current_position);
                    }
                }
                current_position.0 -= 1;
                current_position.1 -= 1;
            }
            while current_position.1 != sensor.1 {
                if current_position.0 <= max_coordinate && current_position.1 <= max_coordinate && current_position.0 >= 0 &&current_position.1 >= 0 && !checked_positions.contains(&current_position) {
                    found = true;
                    for (other_sensor, other_range) in &self.sensor_to_range {
                        if manhattan_distance(&current_position, &other_sensor) < *other_range {
                            checked_positions.insert(current_position);
                            found = false;
                            break;
                        }
                    }
                    if found {
                        return Some(current_position);
                    }
                }
                current_position.0 -= 1;
                current_position.1 += 1;
            }
            while current_position.0 != sensor.0 {
                if current_position.0 <= max_coordinate && current_position.1 <= max_coordinate && current_position.0 >= 0 &&current_position.1 >= 0 && !checked_positions.contains(&current_position) {
                    found = true;
                    for (other_sensor, other_range) in &self.sensor_to_range {
                        if manhattan_distance(&current_position, &other_sensor) < *other_range {
                            checked_positions.insert(current_position);
                            found = false;
                            break;
                        }
                    }
                    if found {
                        return Some(current_position);
                    }
                }
                current_position.0 += 1;
                current_position.1 += 1;
            }
            while current_position.1 != sensor.1 {
                if current_position.0 <= max_coordinate && current_position.1 <= max_coordinate && current_position.0 >= 0 &&current_position.1 >= 0 && !checked_positions.contains(&current_position) {
                    found = true;
                    for (other_sensor, other_range) in &self.sensor_to_range {
                        if manhattan_distance(&current_position, &other_sensor) < *other_range {
                            checked_positions.insert(current_position);
                            found = false;
                            break;
                        }
                    }
                    if found {
                        return Some(current_position);
                    }
                }
                current_position.0 += 1;
                current_position.1 -= 1;
            }
        }
        return None;
    }
}

fn manhattan_distance(point1: &Coordinates, point2: &Coordinates) -> i64 {
    (point1.0 - point2.0).abs() + (point1.1 - point2.1).abs()
}

fn distance_to_horizontal_line(point: &Coordinates, line: i64) -> i64 {
    (point.1 - line).abs()
}


fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let (input, parameter) = contents.split_once("\n\n").unwrap();
    let mut beacon_map = BeaconMap::new();
    for line in input.lines() {
        let sensor_x_start = line.find("x=").unwrap();
        let sensor_x_end = line.find(',').unwrap();
        let sensor_y_start = line.find("y=").unwrap();
        let sensor_y_end = line.find(':').unwrap();
        let sensor_x = line[sensor_x_start+2..sensor_x_end].parse::<i64>().unwrap();
        let sensor_y = line[sensor_y_start+2..sensor_y_end].parse::<i64>().unwrap();

        let beacon_x_start = line.rfind("x=").unwrap();
        let beacon_x_end = line.rfind(',').unwrap();
        let beacon_y_start = line.rfind("y=").unwrap();
        let beacon_y_end = line.len();
        let beacon_x = line[beacon_x_start+2..beacon_x_end].parse::<i64>().unwrap();
        let beacon_y = line[beacon_y_start+2..beacon_y_end].parse::<i64>().unwrap();

        beacon_map.add_sensor_with_beacon(&(sensor_x, sensor_y), &(beacon_x, beacon_y));
    }

    let parameters: Vec<&str> = parameter.lines().collect();

    match part_of_task {
        1 => {
            let parameter = parameters[0].parse::<i64>().unwrap();
            println!("{}", beacon_map.get_beacon_free_positions_along_horizontal_line(parameter).len());
        }
        2 => {
            let parameter = parameters[1].parse::<i64>().unwrap();
            let beacon_position = beacon_map.get_beacon_position_in_area(parameter).unwrap();
            println!("{}", beacon_position.0 * 4000000 + beacon_position.1);
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
