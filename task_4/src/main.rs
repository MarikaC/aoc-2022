use std::fs;
use std::env;

#[derive(Clone, Copy)]
struct SectionRange {
    beginning: u32,
    end: u32
}

impl SectionRange {
    fn new(beginning: u32, end: u32) -> Self {
        SectionRange {
            beginning,
            end
        }
    }
}

fn are_fully_overlapping(range1: &SectionRange, range2: &SectionRange) -> bool {
    (range1.beginning >= range2.beginning && range1.end <= range2.end) ||
    (range1.beginning <= range2.beginning && range1.end >= range2.end)
}

fn are_overlapping(range1: &SectionRange, range2: &SectionRange) -> bool {
    (range1.beginning <= range2.end && range1.beginning >= range2.beginning) ||
    (range1.end <= range2.end && range1.end >= range2.beginning) ||
    (range2.beginning <= range1.end && range2.beginning >= range1.beginning) ||
    (range2.end <= range1.end && range2.end >= range1.beginning) 
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let range_inputs: Vec<&str> = contents.split("\n").collect();

    let mut range_pairs = Vec::<(SectionRange, SectionRange)>::new();
    for input in range_inputs {
        if input.len() > 0 {
            let elfs: Vec<&str> = input.split(",").collect();
            let mut ranges = Vec::<SectionRange>::new();
            for elf in elfs {
                let range_limits: Vec<&str> = elf.split("-").collect();
                ranges.push(SectionRange::new(
                    range_limits[0].parse::<u32>().unwrap(),
                    range_limits[1].parse::<u32>().unwrap()
                ));
            }
            range_pairs.push((ranges[0], ranges[1]));
        }
    }

    match part_of_task {
        1 => {
            let mut overlap_counter = 0;
            for pair in range_pairs {
                if are_fully_overlapping(&pair.0, &pair.1) {
                    overlap_counter += 1;
                }
            }
            println!("{}", overlap_counter);
        }
        2 => {
            let mut overlap_counter = 0;
            for pair in range_pairs {
                if are_overlapping(&pair.0, &pair.1) {
                    overlap_counter += 1;
                }
            }
            println!("{}", overlap_counter);
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
