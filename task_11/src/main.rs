use std::fs;
use std::env;
use std::collections::{VecDeque, HashMap};

trait MonkeyTrait {
    fn catch(&mut self, items: &mut VecDeque<u64>);
    fn inspect_all(&mut self, worry_managed: bool, common_multiple: u64) -> HashMap<usize, VecDeque<u64>>;
    fn get_inspection_counter(&self) -> u64;
    fn get_divider(&self) -> u64;
}

struct Monkey<F: Fn(u64) -> u64> {
    items: VecDeque<u64>,
    operation: F,
    divider: u64,
    target_on_true: usize,
    target_on_false: usize,
    inspection_counter: u64
}

impl<F: Fn(u64) -> u64> Monkey<F> {
    fn new(items: VecDeque<u64>, operation: F, divider: u64, target_on_true: usize, target_on_false: usize) -> Self {
        Monkey {
            items,
            operation,
            divider,
            target_on_true,
            target_on_false,
            inspection_counter: 0
        }
    }

    fn inspect_one(&mut self, worry_managed: bool, common_multiple: u64) -> (usize, u64) {
        self.inspection_counter += 1;
        let mut item = self.items.pop_front().unwrap();
        item = (self.operation)(item);
        if worry_managed {
            item /= 3;
        }
        item %= common_multiple;
        let target =  if item % self.divider == 0 {
            self.target_on_true
        }
        else {
            self.target_on_false
        };
        return (target, item);
    }
}

impl<F: Fn(u64) -> u64> MonkeyTrait for Monkey<F> {
    fn catch(&mut self, items: &mut VecDeque<u64>) {
        self.items.append(items);
    }

    fn inspect_all(&mut self, worry_managed: bool, common_multiple: u64) -> HashMap<usize, VecDeque<u64>> {
        let mut result = HashMap::<usize, VecDeque<u64>>::new();
        while !self.items.is_empty() {
            let (target, item) = self.inspect_one(worry_managed, common_multiple);
            if !result.contains_key(&target) {
                result.insert(target, VecDeque::new());
            }
            result.get_mut(&target).unwrap().push_back(item);
        }
        return result;
    }

    fn get_inspection_counter(&self) -> u64 {
        self.inspection_counter
    }

    fn get_divider(&self) -> u64 {
        self.divider
    }
}

fn parse_operation(elements: Vec<&str>) -> Box<dyn Fn(u64) -> u64> {
    let operation = elements[1];
    if elements[0] == "old" && elements[2] == "old" {
        match operation {
            "+" => {
                return Box::new(|input| &input + &input);
            }
            "-" => {
                return Box::new(|input| &input - &input);
            }
            "*" => {
                return Box::new(|input| &input * &input);
            }
            "/" => {
                return Box::new(|input| &input / &input);
            }
            _ => {
                panic!("Unknown operation {}", operation);
            }
        }
    }
    else if elements[0] == "old" {
        let second_element = elements[2].parse::<u64>().unwrap();
        match operation {
            "+" => {
                return Box::new(move |input| input + second_element.clone());
            }
            "-" => {
                return Box::new(move |input| input - second_element.clone());
            }
            "*" => {
                return Box::new(move |input| input * second_element.clone());
            }
            "/" => {
                return Box::new(move |input| input / second_element.clone());
            }
            _ => {
                panic!("Unknown operation {}", operation);
            }
        }
    }
    else if elements[2] == "old" {
        let first_element = elements[0].parse::<u64>().unwrap();
        match operation {
            "+" => {
                return Box::new(move |input| first_element.clone() + input);
            }
            "-" => {
                return Box::new(move |input| first_element.clone() - input);
            }
            "*" => {
                return Box::new(move |input| first_element.clone() * input);
            }
            "/" => {
                return Box::new(move |input| first_element.clone() / input);
            }
            _ => {
                panic!("Unknown operation {}", operation);
            }
        }
    }
    else {
        let first_element = elements[0].parse::<u64>().unwrap();
        let second_element = elements[2].parse::<u64>().unwrap();
        match operation {
            "+" => {
                return Box::new(move |_| first_element.clone() + second_element.clone());
            }
            "-" => {
                return Box::new(move |_| first_element.clone() - second_element.clone());
            }
            "*" => {
                return Box::new(move |_| first_element.clone() * second_element.clone());
            }
            "/" => {
                return Box::new(move |_| first_element.clone() / second_element.clone());
            }
            _ => {
                panic!("Unknown operation {}", operation);
            }
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let monkey_inputs: Vec<&str> = contents.split("\n\n").collect();
    let mut monkeys = Vec::<Box<dyn MonkeyTrait>>::new();

    for monkey_input in monkey_inputs {
        let input_lines: Vec<&str> = monkey_input.split('\n').collect();
        let (_, items_input) = input_lines[1].split_once(": ").unwrap();
        let items: VecDeque<u64> =  items_input.split(", ").map(|x| x.parse::<u64>().unwrap()).collect();
        let (_, operation_input) = input_lines[2].split_once("= ").unwrap();
        let operation_elements: Vec<&str> = operation_input.split(' ').collect();
        let operation = parse_operation(operation_elements);
        let (_, divider) = input_lines[3].rsplit_once(' ').unwrap();
        let divider = divider.parse::<u64>().unwrap();
        let (_, target_on_true) = input_lines[4].rsplit_once(' ').unwrap();
        let target_on_true = target_on_true.parse::<usize>().unwrap();
        let (_, target_on_false) = input_lines[5].rsplit_once(' ').unwrap();
        let target_on_false = target_on_false.parse::<usize>().unwrap();
        monkeys.push(Box::new(Monkey::new(items, operation, divider, target_on_true, target_on_false)));
    }
    let mut common_multiple = 1;
    for monkey in &monkeys {
        common_multiple *= monkey.get_divider();
    }

    match part_of_task {
        1 => {
            for _ in 0..20 {
                for monkey_index in 0..monkeys.len() {
                    let thrown_items = monkeys[monkey_index].inspect_all(true, common_multiple);
                    for (target, mut item) in thrown_items {
                        monkeys[target].catch(&mut item);
                    }
                }
            }
            monkeys.sort_by(|monkey1, monkey2| monkey1.get_inspection_counter().partial_cmp(&monkey2.get_inspection_counter()).unwrap());
            println!("{}", monkeys[monkeys.len() - 1].get_inspection_counter() * monkeys[monkeys.len() - 2].get_inspection_counter())
        }
        2 => {
            for _ in 0..10000 {
                for monkey_index in 0..monkeys.len() {
                    let thrown_items = monkeys[monkey_index].inspect_all(false, common_multiple);
                    for (target, mut item) in thrown_items {
                        monkeys[target].catch(&mut item);
                    }
                }
            }
            monkeys.sort_by(|monkey1, monkey2| monkey1.get_inspection_counter().partial_cmp(&monkey2.get_inspection_counter()).unwrap());
            println!("{}", monkeys[monkeys.len() - 1].get_inspection_counter() * monkeys[monkeys.len() - 2].get_inspection_counter())
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
