use std::fs;
use std::env;
use std::collections::{VecDeque, HashMap};

struct MoveCommand {
    number_of_crates: u32,
    from: u32,
    to: u32
}

fn perform_single_move_command(stacks: &mut Vec::<VecDeque<char>>, command: &MoveCommand) {
    for _i in 0..command.number_of_crates {
        let moved = stacks[command.from as usize].pop_back().unwrap();
        stacks[command.to as usize].push_back(moved);
    }
}

fn perform_multiple_move_command(stacks: &mut Vec::<VecDeque<char>>, command: &MoveCommand) {
    let mut moved_buffer = VecDeque::<char>::new();
    for _i in 0..command.number_of_crates {
        moved_buffer.push_front(stacks[command.from as usize].pop_back().unwrap());
    }
    for moved in moved_buffer {
        stacks[command.to as usize].push_back(moved);
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let input_parts: Vec<&str> = contents.split("\n\n").collect();
    let stacks_input: Vec<&str> = input_parts[0].split("\n").collect();
    let moves_input: Vec<&str> = input_parts[1].split("\n").collect();

    let mut stack_locations = HashMap::<usize, u32>::new();
    let mut stacks = Vec::<VecDeque<char>>::new();
    let position_input = stacks_input.last().unwrap();
    let stacks_input = stacks_input[..stacks_input.len() - 1].to_vec();

    for i in 1.. {
        if let Some(position) = position_input.find(&i.to_string()) {
            stack_locations.insert(position, i as u32);
        }
        else {
            stacks.resize(i, VecDeque::<char>::new());
            break;
        }
    }

    for line in stacks_input.iter().rev() {
        for character in line.char_indices() {
            let (index, cargo) = character;
            if !cargo.is_alphabetic() {
                continue;
            }
            let stack_number = stack_locations[&index];
            stacks[stack_number as usize].push_back(cargo);
        }
    }

    let mut move_commands = Vec::<MoveCommand>::new();
    for line in moves_input {
        let line_trimmed: String = line.chars().filter(|character| !character.is_alphabetic()).collect();
        let input_parts: Vec<&str> = line_trimmed.split_whitespace().collect();
        let number_of_crates = input_parts[0].parse::<u32>().unwrap();
        let from = input_parts[1].parse::<u32>().unwrap();
        let to = input_parts[2].parse::<u32>().unwrap();
        move_commands.push(MoveCommand{
            number_of_crates,
            from,
            to
        });
    }

    match part_of_task {
        1 => {
            for command in move_commands {
                perform_single_move_command(&mut stacks, &command);
            }

            for stack in stacks {
                if let Some(stack_top) = stack.back() {
                    print!("{}", stack_top);
                }
            }
            println!();
        }
        2 => {
            for command in move_commands {
                perform_multiple_move_command(&mut stacks, &command);
            }

            for stack in stacks {
                if let Some(stack_top) = stack.back() {
                    print!("{}", stack_top);
                }
            }
            println!();
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
