use std::fs;
use std::env;
use std::collections::HashSet;

#[derive(Clone, Copy)]
enum Direction {
    Up,
    Down,
    Left,
    Right
}

type Coordinates = (i32, i32);

struct RopePosition {
    head: Coordinates,
    segments: Vec<Coordinates>
}

impl RopePosition {
    fn new(length: usize) -> Self {
        RopePosition {
            head: (0, 0),
            segments: vec![(0, 0); length - 1]
        }
    }

    fn perform_move(&mut self, direction: Direction) {
        match direction {
            Direction::Up => self.head.1 += 1,
            Direction::Down => self.head.1 -= 1,
            Direction::Left => self.head.0 -= 1,
            Direction::Right => self.head.0 += 1
        }
        let mut previous_segment = self.head;
        for segment in &mut self.segments {
            if !RopePosition::follow_segment(segment, &previous_segment) {
                break;
            }
            previous_segment = segment.clone();
        }
    }

    fn follow_segment(following_segment: &mut Coordinates, followed_segment: &Coordinates) -> bool  {
        let difference = (followed_segment.0 - following_segment.0, followed_segment.1 - following_segment.1);
        if difference.0 == 0 {
            if difference.1 > 1 {
                following_segment.1 += 1;
            }
            else if difference.1 < -1 {
                following_segment.1 -= 1;
            }
            return true;
        }
        else if difference.1 == 0 {
            if difference.0 > 1 {
                following_segment.0 += 1;
            }
            else if difference.0 < -1 {
                following_segment.0 -= 1;
            }
            return true;
        }
        else if difference.0.abs() > 1 || difference.1.abs() > 1 {
            if difference.1 > 0 {
                following_segment.1 += 1;
            }
            else {
                following_segment.1 -= 1;
            }
            if difference.0 > 0 {
                following_segment.0 += 1;
            }
            else {
                following_segment.0 -= 1;
            }
            return true;
        }
        return false;
    }

    fn perform_moves_get_tail_positions(&mut self, directions: Vec<Direction>) -> HashSet<Coordinates> {
        let mut result: HashSet<Coordinates> = vec![*self.segments.last().unwrap()].into_iter().collect();
        for direction in directions {
            self.perform_move(direction);
            result.insert(*self.segments.last().unwrap());
        }
        return result;
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let direction_lines = contents.lines();
    let mut directions = Vec::<Direction>::new();
    for line in direction_lines {
        let (direction_symbol, move_number) = line.split_once(' ').unwrap();
        let direction = match direction_symbol {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "L" => Direction::Left,
            "R" => Direction::Right,
            _ => panic!("Unknown direction")
        };
        for _ in 0..move_number.parse::<u32>().unwrap() {
            directions.push(direction);
        }
    }

    match part_of_task {
        1 => {
            let mut rope_position = RopePosition::new(2);
            let positions = rope_position.perform_moves_get_tail_positions(directions);
            println!("{}", positions.len());
        }
        2 => {
            let mut rope_position = RopePosition::new(10);
            let positions = rope_position.perform_moves_get_tail_positions(directions);
            println!("{}", positions.len());
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
