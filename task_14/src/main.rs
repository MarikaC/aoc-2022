use std::fs;
use std::env;
use std::collections::HashSet;
use std::cmp::{min, max};

type Coordinates = (i64, i64);

struct Cave {
    sand_source: Coordinates,
    occupied_spaces: HashSet<Coordinates>,
    floor_level: i64
}

impl Cave {
    fn new() -> Self {
        Cave {
            sand_source: (500, 0),
            occupied_spaces: HashSet::new(),
            floor_level: 0
        }
    }

    fn add_wall(&mut self, beginning: &Coordinates, end: &Coordinates) {
        if beginning.0 == end.0 {
            let new_beginning = (beginning.0, min(beginning.1, end.1));
            let new_end = (beginning.0, max(beginning.1, end.1));
            self.add_vertical_wall(&new_beginning, &new_end);
        }
        else {
            let new_beginning = (min(beginning.0, end.0), beginning.1);
            let new_end = (max(beginning.0, end.0), beginning.1);
            self.add_horizontal_wall(&new_beginning, &new_end);
        }
    }

    fn generate_floor(&mut self) {
        self.floor_level += 2;
    }

    fn add_vertical_wall(&mut self, beginning: &Coordinates, end: &Coordinates) {
        for i in beginning.1..=end.1 {
            self.occupied_spaces.insert((beginning.0, i));
        }
        self.floor_level = max(self.floor_level, end.1);
    }

    fn add_horizontal_wall(&mut self, beginning: &Coordinates, end: &Coordinates) {
        for i in beginning.0..=end.0 {
            self.occupied_spaces.insert((i, beginning.1));
        }
        self.floor_level = max(self.floor_level, end.1);
    }

    fn drop_sand(&mut self, is_void: bool) -> Option<Coordinates> {
        let mut sand_location = self.sand_source;
        while sand_location.1 < self.floor_level {
            if !self.try_lower_sand(&mut sand_location, is_void) {
                break;
            }
        }

        if is_void && sand_location.1 >= self.floor_level {
            return None;
        }

        self.occupied_spaces.insert(sand_location);
        return Some(sand_location);
    }

    fn try_lower_sand(&mut self, mut sand_location: &mut Coordinates, is_void: bool) -> bool {
        let mut target_location = sand_location.clone();
        target_location.1 += 1;
        if !is_void && &target_location.1 == &self.floor_level {
            return false;
        }
        else if is_void && &target_location.1 > &self.floor_level {
            return false;
        }
        if !self.occupied_spaces.contains(&target_location) {
            sand_location.0 = target_location.0;
            sand_location.1 = target_location.1;
            return true;
        }
        target_location.0 -= 1;
        if !self.occupied_spaces.contains(&target_location) {
            sand_location.0 = target_location.0;
            sand_location.1 = target_location.1;
            return true;
        }
        target_location.0 += 2;
        if !self.occupied_spaces.contains(&target_location) {
            sand_location.0 = target_location.0;
            sand_location.1 = target_location.1;
            return true;
        }
        return false;
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let input_lines = contents.lines();
    let mut cave = Cave::new();
    for line in input_lines {
        let mut points = line.split("->").into_iter();
        let mut beginning;
        let mut end;
        let point = points.next().unwrap().trim();
        let (pointx, pointy) = point.split_once(',').unwrap();
        beginning = (pointx.parse::<i64>().unwrap(), pointy.parse::<i64>().unwrap());
        for point in points {
            let (pointx, pointy) = point.trim().split_once(',').unwrap();
            end = (pointx.parse::<i64>().unwrap(), pointy.parse::<i64>().unwrap());
            cave.add_wall(&beginning, &end);
            beginning = end;
        }
    }
    match part_of_task {
        1 => {
            let mut sand_resting = 0;
            while cave.drop_sand(true).is_some() {
                sand_resting += 1;
            }
            println!("{}", sand_resting);
        }
        2 => {
            cave.generate_floor();
            let mut sand_resting = 1;
            while cave.drop_sand(false).unwrap() != cave.sand_source {
                sand_resting += 1;
            }
            println!("{}", sand_resting);
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
