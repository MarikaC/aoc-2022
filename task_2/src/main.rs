use std::fs;
use std::env;
use std::collections::HashMap;
use std::cmp::Eq;

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
enum Shape {
    Rock,
    Paper,
    Scissors
}

#[derive(Clone, Copy)]
struct Match {
    my_shape: Shape,
    opponent_shape: Shape,
    result: MatchResult,
    score: u32
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
enum MatchResult {
    Win,
    Draw,
    Lose
}

impl Match {
    fn new_first_strategy(first_letter: &str, second_letter: &str) -> Self {
        let opponent_letter_to_shape = HashMap::from([
            ("A", Shape::Rock),
            ("B", Shape::Paper),
            ("C", Shape::Scissors)
        ]);
        let my_letter_to_shape = HashMap::from([
            ("X", Shape::Rock),
            ("Y", Shape::Paper),
            ("Z", Shape::Scissors)
        ]);
        let opponent_shape = opponent_letter_to_shape[first_letter];
        let my_shape = my_letter_to_shape[second_letter];
        let result = Match::get_result(&opponent_shape, &my_shape);
        let score = Match::get_score(&my_shape, &result);
        return Match {
            my_shape: my_shape,
            opponent_shape: opponent_shape,
            result: result,
            score: score
        };
    }

    fn new_second_strategy(first_letter: &str, second_letter: &str) -> Self {
        let opponent_letter_to_shape = HashMap::from([
            ("A", Shape::Rock),
            ("B", Shape::Paper),
            ("C", Shape::Scissors)
        ]);
        let result_letter_to_result = HashMap::from([
            ("X", MatchResult::Lose),
            ("Y", MatchResult::Draw),
            ("Z", MatchResult::Win)
        ]);
        let opponent_shape = opponent_letter_to_shape[first_letter];
        let result = result_letter_to_result[second_letter];
        let my_shape = Match::get_my_shape(&opponent_shape, &result);
        let score = Match::get_score(&my_shape, &result);
        return Match {
            my_shape: my_shape,
            opponent_shape: opponent_shape,
            result: result,
            score: score
        };
    }

    fn get_result(opponent_shape: &Shape, my_shape: &Shape) -> MatchResult {
        let match_to_result = HashMap::from([
            ((Shape::Rock, Shape::Rock), MatchResult::Draw),
            ((Shape::Rock, Shape::Paper), MatchResult::Win),
            ((Shape::Rock, Shape::Scissors), MatchResult::Lose),
            ((Shape::Paper, Shape::Rock), MatchResult::Lose),
            ((Shape::Paper, Shape::Paper), MatchResult::Draw),
            ((Shape::Paper, Shape::Scissors), MatchResult::Win),
            ((Shape::Scissors, Shape::Rock), MatchResult::Win),
            ((Shape::Scissors, Shape::Paper), MatchResult::Lose),
            ((Shape::Scissors, Shape::Scissors), MatchResult::Draw)
        ]);
        match_to_result[&(*opponent_shape, *my_shape)]
    }

    fn get_my_shape(opponent_shape: &Shape, result: &MatchResult) -> Shape {
        let match_to_result = HashMap::from([
            ((Shape::Rock, MatchResult::Win), Shape::Paper),
            ((Shape::Rock, MatchResult::Draw), Shape::Rock),
            ((Shape::Rock, MatchResult::Lose), Shape::Scissors),
            ((Shape::Paper, MatchResult::Win), Shape::Scissors),
            ((Shape::Paper, MatchResult::Draw), Shape::Paper),
            ((Shape::Paper, MatchResult::Lose), Shape::Rock),
            ((Shape::Scissors, MatchResult::Win), Shape::Rock),
            ((Shape::Scissors, MatchResult::Draw), Shape::Scissors),
            ((Shape::Scissors, MatchResult::Lose), Shape::Paper)
        ]);
        match_to_result[&(*opponent_shape, *result)]
    }

    fn get_score(my_shape: &Shape, result: &MatchResult) -> u32 {
        let my_shape_to_score = HashMap::from([
            (Shape::Rock, 1),
            (Shape::Paper, 2),
            (Shape::Scissors, 3)
        ]);

        let result_to_score = HashMap::from([
            (MatchResult::Win, 6),
            (MatchResult::Draw, 3),
            (MatchResult::Lose, 0)
        ]);
        my_shape_to_score[my_shape] + result_to_score[result]
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let matches_input: Vec<&str> = contents.split("\n").collect();

    let mut matches = Vec::<Match>::new();

    match part_of_task {
        1 => {
            for input in matches_input {
                let shapes: Vec<&str> = input.split(" ").collect();
                if shapes.len() == 2 {
                    let first_letter = &shapes[0];
                    let second_letter = &shapes[1];
                    let encounter = Match::new_first_strategy(first_letter, second_letter);
                    matches.push(encounter);
                }
            }
        }
        2 => {
            for input in matches_input {
                let shapes: Vec<&str> = input.split(" ").collect();
                if shapes.len() == 2 {
                    let first_letter = &shapes[0];
                    let second_letter = &shapes[1];
                    let encounter = Match::new_second_strategy(first_letter, second_letter);
                    matches.push(encounter);
                }
            }
        }
        _ => {
            panic!("Wrong task part");
        }
    };
    let mut overall_score: u32 = 0;
    for encounter in matches {
        overall_score += encounter.score;
    }
    println!("{}", overall_score);
}
