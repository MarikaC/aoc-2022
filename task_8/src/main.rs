use std::fs;
use std::env;

fn check_visibility_top(trees: &Vec<Vec<u8>>, i: usize, j:usize) -> bool {
    let current_tree = trees[i][j];
    for checked in 0..i {
        if trees[checked][j] >= current_tree {
            return false;
        }
    }
    return true;
}

fn check_visibility_bottom(trees: &Vec<Vec<u8>>, i: usize, j:usize) -> bool {
    let current_tree = trees[i][j];
    for checked in i+1..trees.len() {
        if trees[checked][j] >= current_tree {
            return false;
        }
    }
    return true;
}

fn check_visibility_left(trees: &Vec<Vec<u8>>, i: usize, j:usize) -> bool {
    let current_tree = trees[i][j];
    for checked in 0..j {
        if trees[i][checked] >= current_tree {
            return false;
        }
    }
    return true;
}

fn check_visibility_right(trees: &Vec<Vec<u8>>, i: usize, j:usize) -> bool {
    let current_tree = trees[i][j];
    for checked in j+1..trees[i].len() {
        if trees[i][checked] >= current_tree {
            return false;
        }
    }
    return true;
}

fn scenic_score_top(trees: &Vec<Vec<u8>>, i: usize, j:usize) -> u32 {
    let current_tree = trees[i][j];
    if i == 0 {
        return 0;
    }
    let mut score = 1;
    for checked in (1..i).rev() {
        if trees[checked][j] < current_tree {
            score += 1;
        }
        else {
            break;
        }
    }
    return score;
}

fn scenic_score_bottom(trees: &Vec<Vec<u8>>, i: usize, j:usize) -> u32 {
    let current_tree = trees[i][j];
    if i == trees.len() - 1 {
        return 0;
    }
    let mut score = 1;
    for checked in i+1..trees.len()-1 {
        if trees[checked][j] < current_tree {
            score += 1;
        }
        else {
            break;
        }
    }
    return score;
}

fn scenic_score_left(trees: &Vec<Vec<u8>>, i: usize, j:usize) -> u32 {
    let current_tree = trees[i][j];
    if j == 0 {
        return 0;
    }
    let mut score = 1;
    for checked in (1..j).rev() {
        if trees[i][checked] < current_tree {
            score += 1;
        }
        else {
            break;
        }
    }
    return score;
}

fn scenic_score_right(trees: &Vec<Vec<u8>>, i: usize, j:usize) -> u32 {
    let current_tree = trees[i][j];
    if j == trees[i].len() - 1 {
        return 0;
    }
    let mut score = 1;
    for checked in j+1..trees[i].len()-1 {
        if trees[i][checked] < current_tree {
            score += 1;
        }
        else {
            break;
        }
    }
    return score;
}

fn count_visible(trees: &Vec<Vec<u8>>) -> u32 {
    let mut result = 0;
    for i in 0..trees.len() {
        for j in 0..trees[i].len() {
            if check_visibility_top(&trees, i, j)
                || check_visibility_bottom(&trees, i, j)
                || check_visibility_left(&trees, i, j)
                || check_visibility_right(&trees, i, j) {
                result += 1;
            }
        }
    }
    return result;
}

fn get_scenic_score(trees: &Vec<Vec<u8>>, i: usize, j:usize) -> u32 {
    scenic_score_top(&trees, i, j) * scenic_score_bottom(&trees, i, j) * scenic_score_left(&trees, i, j) * scenic_score_right(&trees, i, j)
}

fn get_max_scenic_score(trees: &Vec<Vec<u8>>) -> u32 {
    let mut score = 0;
    for i in 0..trees.len() {
        for j in 0..trees[i].len() {
            let current_score = get_scenic_score(&trees, i, j);
            if current_score > score {
                score = current_score;
            }
        }
    }
    return score;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let tree_lines = contents.lines();
    let mut trees = Vec::<Vec<u8>>::new();
    for line in tree_lines {
        trees.push(line.as_bytes().to_vec());
    }

    match part_of_task {
        1 => {
            println!("{}", count_visible(&trees));
        }
        2 => {
            println!("{}", get_max_scenic_score(&trees));
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}