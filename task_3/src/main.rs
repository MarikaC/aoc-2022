// This code is not optimised at all because I had very limited time. A lot of operations there should be performed differently.

use std::fs;
use std::env;
use std::collections::HashSet;

#[derive(Clone)]
struct Rucksack {
    first_compartment: Vec<u8>,
    second_compartment: Vec<u8>
}

impl Rucksack {
    fn new(first_part: &[u8], second_part: &[u8]) -> Self {
        let mut first_compartment = first_part.to_vec();
        first_compartment.sort();
        let mut second_compartment = second_part.to_vec();
        second_compartment.sort();
        Rucksack {
            first_compartment,
            second_compartment
        }
    }

    fn get_common_elements(&self) -> Vec<u8> {
        let mut iter1 = self.first_compartment.iter();
        let mut iter2 = self.second_compartment.iter();
        let mut result = HashSet::<u8>::new();
        let mut first_value = iter1.next();
        let mut second_value = iter2.next();
        while first_value != None && second_value != None {
            if first_value == second_value {
                result.insert(*first_value.unwrap());
                first_value = iter1.next();
                second_value = iter2.next();
            }
            else if first_value < second_value {
                first_value = iter1.next();
            }
            else {
                second_value = iter2.next();
            }
        }
        return result.into_iter().collect();
    }

    fn get_content(&self) -> Vec<u8> {
        let mut result = self.first_compartment.clone();
        result.append(&mut self.second_compartment.clone());
        result.sort();
        return result;
    }
}

fn element_value(element: &u8) -> u32 {
    if *element > 64 && *element < 91 {
        (*element - 64 + 26) as u32
    }
    else if *element > 96 && *element < 123 {
        (*element - 96) as u32
    }
    else {
        panic!("Invalid item");
    }
}

fn common_in_rucksacks(rucksacks: &[Rucksack]) -> Vec<u8> {
    let mut contents = Vec::<Vec<u8>>::new();
    for rucksack in rucksacks {
        contents.push(rucksack.get_content());
    }
    let mut common_elements = HashSet::<u8>::new();
    for element in &contents[0] {
        if contents.iter().all(|content| content.contains(element)) {
            common_elements.insert(*element);
        }
    }
    return common_elements.into_iter().collect();
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let rucksack_inputs: Vec<&str> = contents.split("\n").collect();

    let mut rucksacks = Vec::<Rucksack>::new();
    for input in rucksack_inputs {
        if input.len() > 0 {
            let letters = input.as_bytes();
            let separation = letters.len() / 2;
            let rucksack = Rucksack::new(&letters[..separation], &letters[separation..]);
            rucksacks.push(rucksack);
        }
    }

    match part_of_task {
        1 => {
            let mut common_elements = Vec::<u8>::new();
            for rucksack in rucksacks {
                common_elements.append(&mut rucksack.get_common_elements());
            }
            let mut result = 0;
            for element in common_elements {
                result += element_value(&element);
                println!("{}", element as char);
            }
            println!("{}", result);
        }
        2 => {
            let mut common_elements = Vec::<u8>::new();
            for i in 0..rucksacks.len() {
                if i % 3 != 0 {
                    continue;
                }
                let mut elf_team = Vec::<Rucksack>::new();
                for j in i..i+3 {
                    elf_team.push(rucksacks[j].clone())
                }
                common_elements.append(&mut common_in_rucksacks(&elf_team));
            }
            let mut result = 0;
            for element in common_elements {
                result += element_value(&element);
                println!("{}", element as char);
            }
            println!("{}", result);
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
