use std::fs;
use std::env;

fn find_start_marker(code: &str, marker_size: usize) -> usize {
    let mut not_analyzed_part = code;
    let mut characters_analyzed: usize = 0;
    while not_analyzed_part.len() >= marker_size {
        let duplicate = check_for_duplicates(&not_analyzed_part[..marker_size]);
        if let Some(characters_to_skip) = duplicate {
            let (_, new_not_analyzed_part) = not_analyzed_part.split_at(characters_to_skip);
            not_analyzed_part = new_not_analyzed_part;
            characters_analyzed += characters_to_skip;
        }
        else {
            return characters_analyzed + marker_size;
        }
    }
    return 0;
}

// returns position of first character that has a duplicate indexing from 1
fn check_for_duplicates(text: &str) -> Option<usize> {
    for (i, character) in text.char_indices() {
        if text.rfind(character).unwrap() != i {
            return Some(i + 1);
        }
    }
    return None;
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let codes = contents.lines();

    match part_of_task {
        1 => {
            for code in codes {
                let start_marker = find_start_marker(&code, 4);
                println!("{}", start_marker);
            }
        }
        2 => {
            for code in codes {
                let start_marker = find_start_marker(&code, 14);
                println!("{}", start_marker);
            }
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
