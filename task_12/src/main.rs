use std::fs;
use std::env;
use std::collections::{HashSet, HashMap};
use std::rc::Rc;
use core::cell::RefCell;

#[derive(PartialEq, Clone)]
enum CellType {
    Start,
    End,
    Regular(u8)
}

#[derive(Clone)]
struct GridCell {
    cell_type: CellType,
    reachaable_from: HashSet<(i64, i64)>,
    distance_to_end: Option<u64>
}

impl GridCell {
    fn new(cell_type: &CellType) -> Self {
        GridCell {
            cell_type: cell_type.clone(),
            reachaable_from: HashSet::new(),
            distance_to_end: None
        }
    }

    // checks if self is reachable from other
    fn is_reachable(&self, other_cell: &GridCell) -> bool {
        match self.cell_type {
            CellType::Start => {
                false
            },
            CellType::End => {
                other_cell.cell_type == CellType::Regular('z' as u8)
            }
            CellType::Regular(height) => {
                match other_cell.cell_type {
                    CellType::Start => {
                        height == 'a' as u8
                    },
                    CellType::End => {
                        false
                    },
                    CellType::Regular(other_height) => {
                        height <= (other_height + 1)
                    }
                }
            }
        }
    }
}

struct Grid {
    cells: HashMap<(i64, i64), Rc<RefCell<GridCell>>>,
    end_coordinates: Option<(i64, i64)>
}

impl Grid {
    fn new() -> Self {
        Grid {
            cells: HashMap::new(),
            end_coordinates: None
        }
    }

    fn add_cell(&mut self, coordinates: (i64, i64), cell_type: CellType) {
        self.cells.insert(coordinates,
            Rc::<RefCell<GridCell>>::new(RefCell::<GridCell>::new(GridCell::new(&cell_type))));
        if cell_type == CellType::End {
            self.end_coordinates = Some(coordinates);
        }
    }

    fn generate_reachability(&mut self) {
        for (coordinates, cell) in &self.cells {
            for neighbour_coordinates in Grid::possible_neighbour_coordinates(&coordinates) {
                if let Some(neighbour) = self.cells.get(&neighbour_coordinates) {
                    let mut cell = cell.borrow_mut();
                    let neighbour = neighbour.borrow();
                    if cell.is_reachable(&neighbour) {
                        cell.reachaable_from.insert(neighbour_coordinates);
                    }
                }
            }
        }
    }

    fn bfs_from_end_to_start(&mut self) -> u64 {
        let mut current_distance = 1;
        let mut cells_to_check = self.cells[&self.end_coordinates.unwrap()].borrow().reachaable_from.clone();
        let mut start_reached = false;
        'outer: while !cells_to_check.is_empty() {
            let mut new_cells_to_check = HashSet::<(i64, i64)>::new();
            for cell_coordinates in cells_to_check {
                let mut cell = self.cells[&cell_coordinates].borrow_mut();
                if cell.distance_to_end == None {
                    cell.distance_to_end = Some(current_distance);
                    for reachable_from in &cell.reachaable_from {
                        let reachable_cell = self.cells[&reachable_from].borrow();
                        if reachable_cell.distance_to_end == None {
                            if reachable_cell.cell_type == CellType::Start {
                                start_reached = true;
                                current_distance += 1;
                                break 'outer;
                            }
                            new_cells_to_check.insert(*reachable_from);
                        }
                    }
                }
            }
            current_distance += 1;
            cells_to_check = new_cells_to_check;
        }
        if start_reached {
            return current_distance;
        }
        panic!("Path not found");
    }

    fn bfs_from_end_to_lowest(&mut self) -> u64 {
        let mut current_distance = 1;
        let mut cells_to_check = self.cells[&self.end_coordinates.unwrap()].borrow().reachaable_from.clone();
        let mut start_reached = false;
        'outer: while !cells_to_check.is_empty() {
            let mut new_cells_to_check = HashSet::<(i64, i64)>::new();
            for cell_coordinates in cells_to_check {
                let mut cell = self.cells[&cell_coordinates].borrow_mut();
                if cell.distance_to_end == None {
                    cell.distance_to_end = Some(current_distance);
                    for reachable_from in &cell.reachaable_from {
                        let reachable_cell = self.cells[&reachable_from].borrow();
                        if reachable_cell.distance_to_end == None {
                            if reachable_cell.cell_type == CellType::Start || reachable_cell.cell_type == CellType::Regular('a' as u8) {
                                start_reached = true;
                                current_distance += 1;
                                break 'outer;
                            }
                            new_cells_to_check.insert(*reachable_from);
                        }
                    }
                }
            }
            current_distance += 1;
            cells_to_check = new_cells_to_check;
        }
        if start_reached {
            return current_distance;
        }
        panic!("Path not found");
    }

    fn possible_neighbour_coordinates(coordinates: &(i64, i64)) -> Vec<(i64, i64)> {
        vec![(coordinates.0 - 1, coordinates.1), (coordinates.0 + 1, coordinates.1), (coordinates.0, coordinates.1 - 1), (coordinates.0, coordinates.1 + 1)]
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let input_lines = contents.lines();
    let mut grid = Grid::new();
    let mut line_number = 0;
    for line in input_lines {
        for (index, character) in line.char_indices() {
            let cell_type = match character {
                'S' => CellType::Start,
                'E' => CellType::End,
                letter => CellType::Regular(letter as u8)
            };
            grid.add_cell((line_number, index as i64), cell_type);
        }
        line_number += 1;
    }
    grid.generate_reachability();
    match part_of_task {
        1 => {
            println!("{}", grid.bfs_from_end_to_start());
        }
        2 => {
            println!("{}", grid.bfs_from_end_to_lowest());
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
