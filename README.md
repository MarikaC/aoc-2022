My solutions for Advent of Code 2022 event. I'm planning to do all of the tasks in Rust but if I don't have enough time for it I might switch to Python. :)

Note: The code here is not meant to be pretty. It's meant to solve problems and let me practice coding using tools I don't use on day to day basis.
