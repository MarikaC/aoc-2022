use std::fs;
use std::env;
use std::cmp::Ordering;

#[derive(PartialEq, Eq, Clone, Ord)]
enum Packet {
    Array(Vec<Packet>),
    Number(i64)
}

fn create_packet_from_text(text: &str) -> (Packet, usize) {
    let mut characters_parsed = 0;
    let mut packet = Vec::<Packet>::new();
    while characters_parsed < text.len() {
        match text.as_bytes()[characters_parsed] as char {
            '[' => {
                characters_parsed += 1;
                let (inner_packet, characters_to_skip) = create_packet_from_text(&text[characters_parsed..]);
                characters_parsed += characters_to_skip;
                packet.push(inner_packet);
            }
            ',' => {
                characters_parsed += 1;
            }
            ']' => {
                characters_parsed += 1;
                break;
            }
            _ => {
                let (number, _) = text[characters_parsed..].split_once(&[',', ']'][..]).unwrap();
                characters_parsed += number.len();
                packet.push(Packet::Number(number.parse().unwrap()))
            }
        }
    }
    return (Packet::Array(packet), characters_parsed);
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Packet) -> Option<Ordering> {
        if let Packet::Number(number) = self {
            if let Packet::Number(other_number) = other {
                return number.partial_cmp(other_number);
            }
            else {
                return Packet::Array(vec![self.clone()]).partial_cmp(other);
            }
        }
        else if let Packet::Array(array) = self {
            if let Packet::Array(other_array) = other {
                let mut iter1 = array.iter();
                let mut iter2 = other_array.iter();
                loop {
                    let value1 = iter1.next();
                    let value2 = iter2.next();
                    if value1.is_some() && value2.is_some() {
                        let comparison = value1.unwrap().partial_cmp(value2.unwrap());
                        if comparison.unwrap() == Ordering::Equal {
                            continue;
                        }
                        return comparison;
                    }
                    else if value1.is_some() && value2.is_none() {
                        return Some(Ordering::Greater);
                    }
                    else if value1.is_none() && value2.is_some() {
                        return Some(Ordering::Less);
                    }
                    else {
                        return Some(Ordering::Equal);
                    }
                }
            }
            else {
                return self.partial_cmp(&Packet::Array(vec![other.clone()]));
            }
        }
        else {
            panic!("Something weird happened!");
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let file_path = &args[1];
    let part_of_task: i32 = *&args[2].parse().unwrap();
    let contents = fs::read_to_string(file_path).unwrap();
    let input_pairs = contents.split("\n\n");
    let mut packet_pairs = Vec::<(Packet, Packet)>::new();
    for input_pair in input_pairs {
        let (first_line, second_line) = input_pair.split_once('\n').unwrap();
        packet_pairs.push((create_packet_from_text(first_line).0, create_packet_from_text(second_line).0));
    }
    match part_of_task {
        1 => {
            let mut ordered_index_sum = 0;
            let mut index = 0;
            for packet_pair in packet_pairs {
                index += 1;
                if packet_pair.0 < packet_pair.1 {
                    ordered_index_sum += index;
                }
            }
            println!("{}", ordered_index_sum);
        }
        2 => {
            let mut packets = Vec::<Packet>::new();
            for packet_pair in packet_pairs {
                packets.push(packet_pair.0);
                packets.push(packet_pair.1);
            }
            let divider1 = Packet::Array(vec![Packet::Array(vec![Packet::Array(vec![Packet::Number(2)])])]);
            let divider2 = Packet::Array(vec![Packet::Array(vec![Packet::Array(vec![Packet::Number(6)])])]);
            packets.push(divider1.clone());
            packets.push(divider2.clone());
            packets.sort();
            let index1 = packets.iter().position(|value| value == &divider1).unwrap() + 1;
            let index2 = packets.iter().position(|value| value == &divider2).unwrap() + 1;
            println!("{}", index1 * index2);
        }
        _ => {
            panic!("Wrong task part");
        }
    };
}
